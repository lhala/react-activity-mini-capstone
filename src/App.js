import React from 'react';
import { Container } from 'react-bootstrap';
import Home from './pages/Home'
import NavBar from './components/NavBar';
import Gallery from './pages/Gallery';
import Episodes from './components/Episode';
import Cast from './components/Cast';
import Footer from './components/footer';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import './App.css';

export default function App() {

    return (
        <>
        <Router>
               <NavBar/>
                     <Container>
                              <Switch>
                                         <Route exact path ="/" component={Home}/>
                                         <Route exact path ="/gallery" component={Gallery}/>
                                         <Route exact path ="/episodes" component={Episodes}/>
                                         <Route exact path ="/cast" component={Cast}/>

                              </Switch>
                      </Container>
        </Router>
        <Footer/>
 </>
    );
}