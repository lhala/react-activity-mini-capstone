 
import Banner from '../components/Banner';
import Intro from '../components/Intro';
import Video from '../components/Video'
import Gallery from '../components/Gallery';

export default function Home(){
	return(
		<>  
           <h5>Home</h5>	
			<Banner/>
			<Video/>
            <Intro/>
            <Gallery/>

                      
		</>	
	)	
}

