import {Container,Card, Col,Row }from 'react-bootstrap';

export default function Intro(){
	return(
	<>
	<Container fluid>
		<Row>
			<Col md={4} md={6}>
				<Card className="intro">
					<Card.Body>
						<Card.Title>About</Card.Title>
						<Card.Text>
						Shadow and Bone is a Netflix adaptation of the Grisha Trilogy and Six of Crows Duology by Leigh Bardugo. The series, announced on January 10, 2019, was created, written, and executive produced by showrunner Eric Heisserer, and executive produced by Leigh Bardugo, Shawn Levy, Pouya Shahbazian, Dan Levine, Dan Cohen, and Josh Barry for 21 Laps Entertainment.Season 1 was released on April 23, 2021.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col md={4} md={6}>
				<Card className="intro">
					<Card.Body>
						<Card.Title> Synopsis</Card.Title>
						<Card.Text>
						In a world cleaved in two by a massive barrier of perpetual darkness, where unnatural creatures feast on human flesh, a young soldier uncovers a power that might finally unite her country. But as she struggles to hone her power, dangerous forces plot against her. Thugs, thieves, assassins and saints are at war now, and it will take more than magic to survive.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	</Container>	
</>
		)
}