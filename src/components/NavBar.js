//navbar component
import { Navbar , Container,Nav } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
//function describe structure of a component
export default function NavBar(){
	return(
          <>          
        
            <Navbar expand="lg" variant="dark" bg="dark">
			  <Container>
			     <Navbar.Brand as={Link} to="/">Greshiaverse</Navbar.Brand>
			     <Navbar.Toggle aria-controls="basic-navbar-nav"/>
				 <Navbar.Collapse id="basic-navbar-nav">
				    <Nav className="mr-auto">
				    	<Nav.Link as={Link} to="gallery">Gallery</Nav.Link>
				    	<Nav.Link as={Link} to="episodes">Episodes</Nav.Link>
				    	<Nav.Link as={Link} to="cast">Cast</Nav.Link>
                    </Nav>
                                                
				 </Navbar.Collapse>
			  </Container>
		</Navbar>
           </> 

          )
}