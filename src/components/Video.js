
import React from 'react';
import ReactPlayer from "react-player";
import {Container , Row, Col} from  'react-bootstrap'

 
function Video() {
  return (
  <>	
 	<Container fluid>	
 	  <Row>	 
 	  	<Col md={{ span:9, offset:3}}>
    		<div className="mt-3 pt-3 mb-3">
    			<ReactPlayer url="https://www.youtube.com/watch?v=b1WHQTbJ7vE"/>
    	  		<h3><a href="https://www.youtube.com/watch?v=b1WHQTbJ7vE">&nbsp; &nbsp;Shadow and Bone | Official Trailer | Netflix &nbsp;</a></h3>
    	  	</div>
      	</Col>
      
       </Row> 
    </Container>    
   </> 
  );
}
 
export default Video;