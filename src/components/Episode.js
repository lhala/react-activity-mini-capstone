import { Card } from 'react-bootstrap';
import PropTypes from 'prop-types' //buil-in function of react

 const Episode = ({episode})=>{
	const {name, description,link } = episode;
return(
		<Card>
		  <Card.Body>
		  	 <Card.Title>{name}</Card.Title>
		  	 <Card.Text>
		  	 	<span></span>
		  	 	<br />
		  	 	{description}
		  	 	<br />
		  	 	<span></span>
		  	 	<br/>
		  	 	{link}
		  	 </Card.Text>
		  </Card.Body>
		</Card>
	)
}

export default Episode;

Episode.propTypes={
	episode:PropTypes.shape({
		name:PropTypes.string,
		description:PropTypes.string,
		link:PropTypes.string

	})
}